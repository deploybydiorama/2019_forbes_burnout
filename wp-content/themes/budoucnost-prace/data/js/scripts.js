jQuery(document).ready(function ($) {

    //jQuery for bfcache on safari and firefox
    $(window).bind("pageshow", function(event) {
        if (event.originalEvent.persisted) {
            window.location.reload()
        }
    });


    $('body').on('click', '.scroll-down-image', function (e) {
        var target = $(this).closest("section").next();
        if ($(target).length > 0) {
            e.preventDefault();
            $(target).addClass("active");
            $('html,body').animate({scrollTop: $(target).offset().top}, 500);
        }
    });

    $(window).on("scroll", function () {
        //debugger;
        if ($(window).scrollTop() + 1 > $(window).height()) {
            //debugger;
            if ($(".headline").length > 0) {
                if (!$(".headline").hasClass("hidden")) {
                    $(".headline").addClass("hidden");
                }
            }
        } else {
            if ($(".headline").length > 0) {
                $(".headline").removeClass("hidden");
            }
        }
    });

    //https://stackoverflow.com/questions/49500339/cant-prevent-touchmove-from-scrolling-window-on-ios

    /*document.addEventListener('touchmove', function(e) {
    e.preventDefault();
    }, { passive: false });*/

    scrollWait = 0;
    $(document).on('mousewheel DOMMouseScroll', function (e) {
        if (!$(".detail").length > 0) {
            e.preventDefault();
            if (scrollWait < 1) {
                scrollWait++;
                var delta = e.originalEvent.detail < 0 || e.originalEvent.wheelDelta > 0 ? 1 : -1;
                if (delta < 0) {
                    if ($(".headline").hasClass("hidden")) {
                        if ($("section.active").next().is("section")) {
                            $("section.active").fadeOut(700).removeClass("active").next().fadeIn(500).addClass("active");
                        }
                    } /*else {
                        var target = $('.headline');
                        target.addClass("hidden");
                        $('body, html').animate({scrollTop: $(window).height()}, {duration: 450, easing: "swing"});
                        target.removeClass("active").next().addClass("active");
                    }*/
                } else {
                    if ($(".headline").hasClass("hidden")) {
                        if ($("section.active").prev().length > 0) {
                            if ($("section.active").prev().is(".headline")) {
                                var target = $(".headline");
                                $('body, html').animate({scrollTop: 0}, {duration: 450, easing: "swing"});
                                $("section.active").removeClass("active");
                            } else {
                                $("section.active").fadeOut(700).removeClass("active").prev().fadeIn(500).addClass("active");
                            }
                        }
                    } /*else {
                        var target = $(".headline").prev();
                        if (target.length > 0) {
                            e.preventDefault();
                            target.addClass("active");
                            $('html,body').animate({scrollTop: target.offset().top}, 500);
                        }
                    }*/
                }
            }
        }
    });



    setInterval(function () {
        scrollWait = 0;
    }, 700);



    if ($(window).height() > 800) {
        if ($('.headline-inner').length > 0) {
            $('.headline-inner').addClass("top");
        }
    }
    if ($(window).height() < 850) {
        if ($('.headline').length > 0) {
            $('.headline').addClass("short");
        }
        $('body').addClass("short-body");
    }
    $(window).on("resize", function () {
        if ($(window).height() < 415) {
            if ($(".left-bar").length > 0) {
                if (!$(".left-bar").hasClass('hidden')) {
                    $(".left-bar").addClass('hidden');
                }
            }
        } else {
            if ($(".left-bar").length > 0) {
                if ($(".left-bar").hasClass('hidden')) {
                    $(".left-bar").removeClass('hidden');
                }
            }
        }
        var inner = $('.headline-inner');
        if (inner.length > 0) {
            $(window).height() > 800 ? inner.addClass("top") : inner.removeClass("top");
        }
        var headline = $('.headline');
        var bodyEl = $('body');
        if (headline.length > 0) {
            $(window).height() < 850 ? headline.addClass("short") : headline.removeClass("short");
        }
        $(window).height() < 850 ? bodyEl.addClass("short-body") : bodyEl.removeClass("short-body");
    });
    $(document).on("click", ".burger, .back-arrow", function () {
        $(".fixed-menu").toggleClass("visible");
    });
    $(document).on("click", "span.prev", function () {
        var target = $("section.active").prev();
        if (!target.hasClass("headline")) {
            $("section.active").fadeOut(700).removeClass("active").prev().fadeIn(500).addClass("active");
        } else {
            $("section.active").removeClass("active");
        }
        $('html,body').animate({scrollTop: target.offset().top}, 500);
    });
    $(document).on("click", "span.next", function () {
        var target = $("section.active").next();
        $("section.active").fadeOut(700).removeClass("active").next().fadeIn(500).addClass("active");
        $('html,body').animate({scrollTop: target.offset().top}, 500);
    });
    if ($(".scroll-down-image img").length > 0) {
        $(".scroll-down-image img").first().addClass("visible");
        if ($(".scroll-down-image").data("interval").length > 0) {
            var intervalTime = $(".scroll-down-image").data("interval");
        } else {
            var intervalTime = 2000;
        }
        ;setInterval(function () {
            var it = 0;
            $(".scroll-down-image").find("img").each(function () {
                if ($(this).hasClass("visible")) {
                    if (it < 1) {
                        it++;
                        if ($(this).is(":last-child")) {
                            $(this).removeClass("visible").fadeOut(700);
                            $(this).siblings().first().addClass("visible").fadeIn(500);
                        } else {
                            $(this).removeClass("visible").fadeOut(700);
                            $(this).next().addClass("visible").fadeIn(500);
                        }
                    }
                }
            });
        }, intervalTime);
    }
    ;

    $(window).load(function () {
        if ($("body").hasClass("single") || $("body").hasClass("error404") || $("body").hasClass("page-template-template-quiz")) {
            setTimeout(function () {
                $("body").addClass("was-out");
            }, 500);
        }
        $('body, html').animate({scrollTop: 0}, 20);
    });
    $(document).on("click", ".person-inner-cell a, .fixed-menu nav ul li a", function (e) {
        e.preventDefault();
        var link = $(this).attr("href");
        $("body").addClass("out-left");
        setTimeout(function () {
            window.location = link;
        }, 800);
    });
    $(document).on("keyup", function(e) {
        if (e.keyCode == 37) {
            $(".burger").click();
        } else if (e.keyCode == 39) {
            if ($("section.active").length > 0) {
                if ($("section.active").find("a")) {
                    if ($("section.active").find("a").attr("href")) {
                        window.location = $("section.active").find("a").attr("href");
                    }
                }
            }
        } else if (e.keyCode == 38) {
            if (!$(".detail").length > 0) {
                if (!$(".fixed-menu").hasClass("visible")) {
                    if ($(".headline").hasClass("hidden")) {
                        var target = $("section.active").prev();
                        if (!target.hasClass("headline")) {
                            $("section.active").fadeOut(700).removeClass("active").prev().fadeIn(500).addClass("active");
                        } else {
                            $("section.active").removeClass("active");
                        }
                        $('html,body').animate({scrollTop: target.offset().top}, 500);
                    }
                }
            }
        } else if (e.keyCode == 40) {
            if (!$(".detail").length > 0) {
                if (!$(".fixed-menu").hasClass("visible")) {
                    if (!$(".headline").hasClass("hidden")) {
                        var target = $(".headline").next();
                        if (target.length > 0) {
                            target.addClass("active");
                            $('html,body').animate({scrollTop: target.offset().top}, 500);
                        }
                    } else {
                        if ($("section.active").next().is("section")) {
                            var target = $("section.active").next();
                            if (target.length > 0) {
                                $("section.active").fadeOut(700).removeClass("active").next().fadeIn(500).addClass("active");
                                $('html,body').animate({scrollTop: target.offset().top}, 500);
                            }
                        }
                    }
                }
            }
        }
    });
    $("body input[type='text']").on("keyup", function(e) {
        if (e.keyCode === 37) {
            e.stopPropagation();
        }
    });

    if ($(".headline-inner-cell").find("h1").length > 0) {
        var container = $(".headline-inner-cell").find("h1");
        container.shuffleLetters();
    }

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault) e.preventDefault();
        e.returnValue = false;
    }

    function disableScroll() {
        window.ontouchmove = preventDefault;
    }

    if (!$(".detail").length > 0) {
        disableScroll();
    }
    $(window).scroll(function () {
        if (navigator.userAgent.match(/Windows Phone/i)) {
            $("body").addClass("wip-fix");
        }
    });
    if ($(window).height() < 415) {
        if ($(".left-bar").length > 0) {
            if (!$(".left-bar").hasClass('hidden')) {
                $(".left-bar").addClass('hidden');
            }
        }
    }

    if ($(".fixed-menu-special").length > 0) {

        $(".fixed-menu-special").html($(".fixed-menu").html());

    }

    if (!$("body").hasClass("single") && !$("body").hasClass("error404") && !$("body").hasClass("page-template-template-subpage")) {
        $("html").addClass("hidden");
    } else {
        $("html").addClass("easy");
    }

    // $("a.facebook-icon").click(function (e) {
    //
    //     e.preventDefault();
    //
    //     FB.ui({
    //         method: 'feed',
    //         name: document.title,
    //         description: jQuery(this).data("text"),
    //         link: location.href,
    //     }, function (response) {
    //     });
    //
    // });

});