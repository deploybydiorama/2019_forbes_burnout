jQuery(document).ready(function ($) {
    /*-------------------------------------------*/
    /*--slider--*/
    function fnQuiz() {
        var $clSlider = 'questions-slider',
            $clResults = 'results',
            $clIntro = 'intro',
            $clQuestion = 'question',
            $clResultActive = 'active',
            $clIsQuestion = 'is-question',
            $dataPoints = 'points',
            $dataPointsMin = 'points-min',
            $dataPointsMax = 'points-max',
            $dataTotalPoints = 'points-total',
            $elSlider = $('.'+$clSlider);

        $elSlider.slick({
            accessibility: false,
            adaptiveHeight: false,
            arrows: false,
            autoplay: false,
            autoplaySpeed: 1000,
            dots: false,
            draggable: false,
            fade: true,
            infinite: true,
            initialSlide: 0,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: false
        });

        function fnValidateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        //intro form
        function fnRemoveErrorClass(clError){
            $('.'+$clSlider+' .error').each(function () {
                $(this).removeClass(clError);
            });
        }
        $('.'+$clSlider+' .'+$clIntro+' button').on('click',function(e){
            e.preventDefault();
            var $error = $('.intro-form-error'),
                $values = {},
                $fldEmail = $('.intro-form-data-email'),
                $fldGdpr =  $('.intro-form-data-gdpr'),
                $clError =  'error',
                $errorFieldEmail = '<span>Pre pokračovanie vložte prosím váš email.</span>',
                $errorFieldGdpr = '<span>Pre pokračovanie musíte súhlasiť s obchodnými podmienkami.</span>',
                $errorValidationEmail = '<span>Vložte prosím platnú emailovú adresu.</span>';
            $error.html('');
            fnRemoveErrorClass($clError);
            if($fldEmail.length && $fldGdpr.length){
                var $email = $fldEmail.val(),
                    $gdpr =  $fldGdpr.is(':checked');
                if($email == '' || $gdpr==false){
                    if($email == ''){
                        $fldEmail.addClass($clError);
                        $error.append($errorFieldEmail);
                    }
                    if($gdpr==false){
                        $fldGdpr.addClass($clError);
                        $error.append($errorFieldGdpr);
                    }
                }else{
                    if(fnValidateEmail($email)){
                        $values.email = $email;
                        $.post(link.ajax + '?action=quiz_submit',$values,function (data) {
                            if(data.success){
                                $error.html('');
                                fnRemoveErrorClass($clError);
                                $elSlider.slick('slickNext');
                            }else{
                                fnRemoveErrorClass($clError);
                                $error.html($errorValidationEmail);
                            }
                        },'json');
                    }else{
                        $error.html($errorValidationEmail);
                    }
                }
            }
        });

        //disable question buttons first (prevent multiple clicks before slide fade in)
        $('.'+$clSlider+' .'+$clQuestion+' button').each(function(){
            $(this).attr('disabled','disabled');
        });

        $('.'+$clSlider+' .'+$clQuestion+' button').on('click',function(e){
            e.preventDefault();
            var $this = $(this),
                $points = parseInt($this.data($dataPoints)),
                $pointsTotal = parseInt($('.'+$clResults).data($dataTotalPoints));

            if($.isNumeric($points) && $.isNumeric($pointsTotal)){
                $('.'+$clResults).data($dataTotalPoints,($pointsTotal + $points));
            }
            //console.log($pointsTotal);
            $elSlider.slick('slickNext');
        });

        $elSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var $elNextSlide = $(slick.$slides.get(nextSlide)),
                $elQuiz = $('body'),
                $pointsTotal = parseInt($('.'+$clResults).data($dataTotalPoints));
            if($elNextSlide.hasClass($clResults) && $.isNumeric($pointsTotal)){
                $('.'+$clResults+' .result').each(function(){
                   var $this = $(this),
                       $pointsMin = parseInt($this.data($dataPointsMin)),
                       $pointsMax = parseInt($this.data($dataPointsMax));
                   if($.isNumeric($pointsMin) && $.isNumeric($pointsMax)){
                       //console.log($pointsTotal);
                       if($pointsTotal<0){
                           $pointsTotal = 0;
                       }
                       if($pointsTotal>=$pointsMin && $pointsTotal<=$pointsMax){
                           $this.addClass($clResultActive);
                       }
                   }
                });
            }
            if($elNextSlide.hasClass($clQuestion)){
                if(!$elQuiz.hasClass($clIsQuestion)){
                    $elQuiz.addClass($clIsQuestion);
                }
            }else{
                $elQuiz.removeClass($clIsQuestion);
            }
        });

        $elSlider.on('afterChange', function(event, slick, currentSlide){
            var $slide = $(slick.$slides.get(currentSlide)),
                $slideButton = $slide.find('button');
            $slideButton.each(function(){
                $slideButton.removeAttr('disabled');
            });
        });
    }
    fnQuiz();
    /*-------------------------------------------*/
});