jQuery(document).ready(function ($) {
   
   $(document).on('click', '.more-item', function(e){
        e.preventDefault();
        if($(this).prev().length > 0) {
            var targetCode = $(this).prev().html();
            var targetClasses = $(this).prev().attr('class');
            $(this).prev().after('<div class="' + targetClasses + '">' + targetCode + '</div>');
            if($(this).prev().find('.form-serial-number').length > 0) {
                $(this).prev().find('.form-serial-number').each(function(){
                    if(!$(this).is(':first-of-type')) {
                        $(this).remove();
                    }
                });
            }
            if ($(this).prev().find('select').length > 0) {
                $(this).prev().find('select').each(function () {
                    $(this).val('');
                    var sName = $(this).attr('name');
                    var sNr = parseInt(sName.match(/\d/g));
                    var sTargetNr = sNr + 1;
                    var sTargetName = sName.replace(new RegExp("[0-9]", "g"), sTargetNr);
                    $(this).attr('name', sTargetName);
                });
                $(this).prev().find('select').siblings('.select2').remove();
                $(this).prev().find('select').find('option').each(function(){
                    if($(this).is(':first-child')) {
                        $(this).attr('selected',true);
                    }
                    $(this).attr('selected',false);
                });
                $(this).prev().find('select').select2();
            }
            if ($(this).prev().find('input').length > 0) {
                $(this).prev().find('input').each(function () {
                    $(this).val('');
                    var inputName = $(this).attr('name');
                    var inputNr = parseInt(inputName.match(/\d/g));
                    var inputTargetNr = inputNr + 1;
                    var inputTargetName = inputName.replace(new RegExp("[0-9]", "g"), inputTargetNr);
                    $(this).attr('name', inputTargetName);
                });
            }
        }
    });
    
    $('body').on('click','.form-item-remove, .device-form-box-remove', function(e) {
        e.preventDefault();
        $(this).parent().remove();
    });
    
    
        
});