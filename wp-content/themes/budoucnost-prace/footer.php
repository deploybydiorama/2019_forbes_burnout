	<?php
		$logo_forbes = get_field('logo_forbes','options');
		$obrazky = get_field('obrazky');

        $cookies_button = 'Accept';
        $cookies_text = '';
		$cookies_text = get_field('cookies_text','options');
        $cookies_button = get_field('cookies_button','options');
	?>
        <div class="left-bar">
            <div class="left-bar-title"><a href="<?php echo get_site_url()?>"><?php bloginfo('name'); ?></a></div>
			<?php if($logo_forbes) { ?>	
                <a href="<?php echo $logo_forbes['description']; ?>" target="_blank" class="left-bar-logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/data/images/logo-forbes-v.svg" alt="<?php echo $logo_forbes['title']; ?>" />
                </a>
            <?php }; ?>
        </div>






    <div class="cookie-message js-cookies">
        <div class="cookie-message__text"><?php echo $cookies_text; ?></div>
        <button class="cookie-message__button js-cookies__agree"><?php echo $cookies_button; ?></button>
    </div>








		<?php wp_footer(); ?>
    </body>
</html>