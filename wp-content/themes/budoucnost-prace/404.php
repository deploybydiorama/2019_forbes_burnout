<?php get_header(); ?>
        <section class="detail">
            <div class="container">
                <div class="detail-inner">
                    <div class="detail-inner-scroll">
                        <h1><?php _e('Chyba 404','forbes'); ?></h1>
                        <div class="job"><?php _e('Stránka nebola nájdená','forbes'); ?></div>
						<p class="center"><?php _e('Je nám ľúto, ale hľadaný obsah nebol nájdený. Skúste to prosím znova...','forbes'); ?></p>
                    </div>
                </div>
            </div>
        </section>
<?php get_footer(); ?>