<?php

function fnFormSubmit() {
    $eMail = $_POST['email'];
    if ( is_email( $eMail)){
        $submissionId = wp_insert_post([
            'post_title' => "{$eMail}",
            'post_type'  => 'submission',
        ]);
        update_field( 'email', $eMail, $submissionId );
        wp_publish_post( $submissionId );
        $data['success'] = true;
        echo json_encode ( $data );
        die();
    }else{
        $data['success'] = false;
        echo json_encode ( $data );
        die();
    }
}

add_action('wp_ajax_nopriv_quiz_submit', 'fnFormSubmit');
add_action('wp_ajax_quiz_submit', 'fnFormSubmit');