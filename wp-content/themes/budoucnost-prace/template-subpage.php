<?php
/*
Template Name: Samostana stranka
*/
get_header();
$pozice = get_field('pozice');
$tip = get_field('tip');
$nadpis = get_field('nadpis');
?>

        <section class="detail">

            <div class="container">

                <div class="detail-inner">

                        <?php if(have_posts()) { while(have_posts()) { the_post(); ?>

                        <div class="detail-inner-scroll">       
                            
                            <?php $targetNr =  getnmbr(get_the_ID()); ?>
                            
                            <?php if(has_post_thumbnail()) { $thumbnail_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail'); ?>

                            	<div class="image image<?php echo $targetNr; ?>"><img src="<?php echo $thumbnail_image_url[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></div>

                            <?php }; ?>

                            <?php if($nadpis) { ?><h1><?php echo $nadpis; ?></h1><?php }; ?>

                            <div class="content">

                                <?php the_content(); ?>

                            </div>

                            <div class="your-story">
                                <p>Chcete s nami vyzdieľať svoj príbeh? Napíšte nám na <a href="mailto:editori@forbes.sk">editori@forbes.sk</a></p>
                            </div>

                            <?php include('inc-share.php'); ?>



                                </div>

                    <?php };}; ?>

                </div>

            </div>

        </section>

<?php get_footer(); ?>