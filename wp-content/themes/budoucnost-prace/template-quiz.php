<?php
/*
Template Name: Quiz
*/
get_header();
wp_enqueue_script('slick_script');
wp_enqueue_script('quiz_script');
?>

<section class="section-quiz">
    <div class="wrap">
        <div class="questions-slider">
            <div class="item intro">
                <div class="item-inner">
                    <div class="question-num"></div>
                    <div class="item-text">
                        <h3>Hrozí vám vyhorenie?</h3>
                        <h4>Otestujte sa.</h4>
                        <p>Vyhoreniu predchádza veľký zápal a nadšenie, ktoré sa postupne pretavia do vyčerpania. Zistite, či hrozí vyhorenie aj vám pomocou krátkych odpovedí na nasledujúce otázky.</p>
                        <form class="intro-form">
                            <div class="intro-form-row">
                                <input type="text" placeholder="Váš e-mail" class="intro-form-data-email">
                            </div>
                            <div class="intro-form-row">
                                <input id="fld-gdpr" type="checkbox" class="intro-form-data-gdpr">
                                <label for="fld-gdpr">Súhlasím s využitím údajov pre personalizáciou komunikácie v súlade s <a href="https://www.forbes.sk/obchodne-podmienky/" target="_blank">obchodnými podmienkami</a></label>
                            </div>
                            <div class="intro-form-error"></div>
                        </form>
                    </div>
                    <div class="item-cta">
                        <button>Spustiť kvíz</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">01</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Práca je pre mňa náročná, ale vždy nájdem niečo nové a zaujímavé, čo môžem dosiahnuť alebo sa naučiť.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="-5">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">02</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Okrem práce venujem čas aktivitám, ktoré mám rád.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="-3">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">03</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Mám častejšie problém sa sústrediť. Moju koncentráciu pri práci narúšajú aj tie najmenšie maličkosti.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="2">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">04</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Rozčuľujú ma veci, ktoré mi voľakedy neprekážali. Vybuchnem ľahšie ako v minulosti.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="2">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">05</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Mám problémy so zaspávaním, niekedy sa budím v noci a nedarí sa mi opäť zaspať.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="2">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">06</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Cítim sa často unavená/ý, väčšinou sa mi ráno nechce vstať z postele.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="3">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">07</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Často sa mi aj bez zvýšenej telesnej námahy zrýchli pulz.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="2">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">08</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Zabúdam častejšie ako v minulosti.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="2">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">09</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Život mi znepríjemňujú častejšie bolesti hlavy.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="3">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">10</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Prestávam veriť svojim schopnostiam. Mám toho viac, ako sa dá zvládnuť.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="4">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">11</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Mám strach, že prídem o prácu, či o niekoho blízkeho.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="4">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">12</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Vyhýbam sa ľuďom, tráviť čas (aj s blízkymi) ľuďmi je pre mňa čoraz vyčerpávajúcejšie.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="5">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">13</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Mám v poslednom období sklon k cynizmu a ”štipľavému” humoru.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="2">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">14</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Strácam chuť na sex.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="2">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">15</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Niekedy mám pocit prázdnoty.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="5">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">16</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Už dávno som nemal skvelý deň.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="5">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">17</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Čím ďalej, tým menej mi záleží na tom, či prácu alebo pracovné úlohy niekedy dokončím.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="4">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">18</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p> Mám pocit, že sa o mojich problémoch nemám s kým porozprávať.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="3">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">19</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Prestávam vidieť zmysel v mojej práci a v živote.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="5">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item question">
                <div class="item-inner">
                    <div class="question-num">
                        <span class="num">20</span>
                        <span class="from">20</span>
                    </div>
                    <div class="question-text">
                        <p>Čoraz častejšie sa pohrávam s myšlienkou na odchod, mám chuť od všetkého utiecť.</p>
                    </div>
                    <div class="item-cta">
                        <button data-points="5">Áno</button>
                        <button data-points="0">Nie</button>
                    </div>
                </div>
            </div>
            <div class="item results" data-points-total="0">
                <div class="item-inner">
                    <div class="result item-text" data-points-min="0" data-points-max="10">
                        <h3>Nehrozí vám vyhorenie</h3>
                        <p>Zažívate normálnu reakciu na život a dobre zvládate stres. Nie ste ohrozený/á syndrómom vyhorenia. Pokračujte vo svojom prístupe k životu a v spôsobe ako manažujete stres. Zvážte podanie pomoci alebo podpory ostatným okolo vás, u ktorých sa prejavujú príznaky stresu.</p>
                    </div>
                    <div class="result item-text" data-points-min="11" data-points-max="20">
                        <h3>Prvé signály</h3>
                        <p>Začínate zažívať mierne náznaky vyhorenia a možno sa cítite trochu znepokojený/á. Nedovoľte, aby sa úroveň stresu nebadane zvyšovala. Použite techniky zvládania stresu, aby ste proaktívne manažovali stres a zabránili ďalším krokom smerom k vyhoreniu.</p>
                    </div>
                    <div class="result item-text" data-points-min="21" data-points-max="34">
                        <h3>Dávajte pozor</h3>
                        <p>Pravdepodobne máte počiatočné štádium vyhorenia. V minulosti ste možno boli kreatívnejší v nachádzaní riešení a pozitív v ťažších situáciách, ale teraz sa stretávate s určitými obmedzeniami v premýšľaní o ďalších možnostiach. Vaša perspektíva sa môže zúžiť, čo spôsobí, že problémy vidíte horšie, ako sú. Buďte proaktívni pri vedomom používaní techník manažovania stresu.</p>
                    </div>
                    <div class="result item-text" data-points-min="35" data-points-max="45">
                        <h3>Ste ohrozený</h3>
                        <p>Môžete mať vážnejšie prejavy syndrómu vyhorenia. Váš pohľad na pozitivitu a možnosti je obmedzený. Vaša energia, vytrvalosť, fyzická pohoda a schopnosť fungovať môžu byť tiež obmedzené. Hľadajte pomoc od priateľa alebo profesionála, aby ste získali možnosť perspektívy zvonka a pomoc pri praktizovaní techník zvládania a znižovania stresu.</p>
                    </div>
                    <div class="result item-text" data-points-min="46" data-points-max="60">
                        <h3>Je to vážne</h3>
                        <p>Pravdepodobne máte kritické štádium vyhorenia a cítite sa bezmocný/á čokoľvek zmeniť. Hoci existuje nádej a pomoc, môže byť pre vás ťažké to teraz vidieť. Doprajte si okamžitý oddych a oslovte odborníka, lekára alebo poradcu. Buďte rozhodní až agresívni pri znižovaní stresu a zavádzaní zmien k lepšiemu.</p>
                    </div>
                    <div class="item-cta">
                        <a href="<?php echo get_site_url()?>/fazy-vyhorenia" class="btn-look">Viac o fázach vyhorenia</a>
                    </div>
                    <div class="item-link">
                        <a href="<?php echo get_site_url()?>" class="link-look">Hlavná stránka</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
