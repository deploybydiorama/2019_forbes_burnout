<?php
/**
 * Created by PhpStorm.
 * User: dusanjankovic
 * Date: 02/01/2019
 * Time: 15:09
 */

require_once get_stylesheet_directory().'/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

function func_test() {
    wp_mail( 'dusan.jankovic@deploy.sk', 'Automatic email', 'Automatic scheduled email from WordPress to test cron');
}

/*CSV export*/
function deploy_export_submissions() {

    $args = array(
        'posts_per_page'	=> -1,
        'post_type'		=> 'submission'
    );
    $the_query = new WP_Query( $args );

    $filenameXlsx = 'submissions-' . time() . '.xlsx';

    $writer = WriterFactory::create(Type::XLSX);
    $writer->openToFile(get_stylesheet_directory() . '/submision-export/' . $filenameXlsx);
    $writer->addRow(array(
        'Email'
    ));
    if( $the_query->have_posts() ):
        while( $the_query->have_posts() ) : $the_query->the_post();
            $e = get_field('email');
            $writer->addRow(array(
                $e
            ));
        endwhile;
    endif;
    $writer->close();
    print 'Export je pripravený, môžete si ho stiahnuť na <a href="'.get_template_directory_uri() . '/submision-export/' . $filenameXlsx.'">tejto adrese</a>.';
}