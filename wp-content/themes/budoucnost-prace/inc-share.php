<div class="share">

    <?php $share_title = get_bloginfo( 'name' ) . ' - ' . get_the_title(); ?>
    <?php $share = get_the_permalink(); ?>
    <?php $share_text =  get_bloginfo( 'name' ) . ' - ' . get_the_title() . ' | ' . get_the_permalink(); ?>



    <div class="social-icons">

        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($share); ?>" target="_blank" class="social">
            <span class="i-facebook"></span>
        </a>

        <a href="https://twitter.com/home?status=<?php echo urlencode($share); ?>" target="_blank" class="social">
            <span class="i-twitter"></span>
        </a>

        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode($share); ?>&title=<?php echo $share_title; ?>&summary=&source=" target="_blank" class="social">
            <span class="i-linkedin"></span>
        </a>

        <a href="mailto:?subject=<?php echo get_bloginfo( 'name' ); ?>&amp;body=<?php echo $share_text; ?>" target="_blank" class="social">
            <span class="i-email"></span>
        </a>

    </div>

</div>