<?php
require_once('ajax.php');
require_once 'exportCron.php';

if ( function_exists('register_sidebar') ) {
    
	register_sidebar(array('name'=>'Sidebar'));

};

function getnmbr($postID) {
    $temp_query = $wp_query;
    $postNumberQuery = new WP_Query(array( 'post_type' => 'any', 'posts_per_page' => '-1'));
    $counter = 1;
    $postCount = 0;
    if ($postNumberQuery->have_posts()) :
        while ($postNumberQuery->have_posts()) : $postNumberQuery->the_post();
            if ($postID == get_the_ID()) { 
                $postCount = $counter;
            } else {
                $counter++;   
            }
        endwhile;
    endif;
    wp_reset_query();     
    $wp_query = $temp_query;
    return $postCount;
}

add_theme_support('post-thumbnails');


function remove_jquery_migrate( $scripts ) {
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
        $script = $scripts->registered['jquery'];

        if ( $script->deps ) { // Check whether the script has any dependencies
            $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
        }
    }
}

add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

function register_assets() {
    wp_register_script('slick_script',get_template_directory_uri() . '/data/js/slick.min.js', '', '', true );

    wp_enqueue_script('quiz_script',get_template_directory_uri() . '/data/js/quiz.js', array('jquery')  , null, true );
    wp_localize_script( 'quiz_script', 'link', array(
        'ajax' => admin_url( 'admin-ajax.php' )
    ));
}
add_action('wp_enqueue_scripts', 'register_assets');

function remove_menus(){

  remove_menu_page( 'edit-comments.php' );          //Comments

}

add_action( 'admin_menu', 'remove_menus' );

if(function_exists('acf_add_options_page')) {

	acf_add_options_page(array(

		'page_title' 	=> 'Konfigurace webu',

		'menu_title'	=> 'Konfigurace'

		));

};

// Callback function to insert 'styleselect' into the $buttons array

function my_mce_buttons_2( $buttons ) {

	array_unshift( $buttons, 'styleselect' );

	return $buttons;

}

// Register our callback to the appropriate filter

add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );



// Callback function to filter the MCE settings

function my_mce_before_init_insert_formats( $init_array ) {  

	// Define the style_formats array

	$style_formats = array(  

		// Each array child is a format with it's own settings

		array(  

			'title' => 'side block',  

			'block' => 'div',  

			'classes' => 'sideblock',

			'wrapper' => true,

		),  

	);  

	// Insert the array, JSON ENCODED, into 'style_formats'

	$init_array['style_formats'] = json_encode( $style_formats );  

	

	return $init_array;  

  

} 

// Attach callback to 'tiny_mce_before_init' 

add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  



function cc_mime_types($mimes) {

	$mimes['svg'] = 'image/svg+xml';

	return $mimes;

}

add_filter('upload_mimes', 'cc_mime_types');

//DISABLE GUTTENBERG
//disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);
//disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

// custom post types
register_post_type( 'submission',
    array(
        'labels' => array(
            'name' => __( 'Submissions' ),
            'singular_name' => __( 'Submission' )
        ),
        'public' => true,
        'show_ui' => true,
        'supports' => array( 'title' ),
        'menu_icon' => 'dashicons-images-alt',
    )
);

// session start
session_start();

?>