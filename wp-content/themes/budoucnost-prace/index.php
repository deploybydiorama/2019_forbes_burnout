<?php get_header(); ?>
       
	<?php

		$pozice = get_field('pozice');
		$tip = get_field('tip');
		$nadpis = get_field('nadpis');

	?>

        <section class="detail">

            <div class="container">

                <div class="detail-inner">

                        <?php if(have_posts()) { while(have_posts()) { the_post(); ?>

                        <div class="detail-inner-scroll">       
                            
                            <?php $targetNr =  getnmbr(get_the_ID()); ?>
                            
                            <?php if(has_post_thumbnail()) { $thumbnail_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail'); ?>

                            	<div class="image image<?php echo $targetNr; ?>"><img src="<?php echo $thumbnail_image_url[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></div>

                            <?php }; ?>

                            <?php if($nadpis) { ?><h1><?php echo $nadpis; ?></h1><?php }; ?>

                            <h2 class="name"><?php the_title(); ?></h2>
                            <?php if($pozice) { ?><div class="job"><?php echo $pozice; ?></div><?php }; ?>

                            <div class="content">

                                <?php the_content(); ?>

                                <?php if($tip) { ?>
                                    <div class="tip">
                                        <?php echo $tip; ?>
                                    </div>
                                <?php }; ?>

                            </div>

                            <div class="your-story">
                                <p>Chcete s nami vyzdieľať svoj príbeh? Napíšte nám na <a href="mailto:editori@forbes.sk">editori@forbes.sk</a></p>
                            </div>

                            <?php include('inc-share.php'); ?>

                            <div class="arrows">

                                <span class='prev-link arrow<?php echo $targetNr-1; ?>'>
                                            <?php $nextPost = get_next_post(true);
                                            if ($nextPost) {
                                                ?>

                                                <?php $nextthumbnail = get_the_post_thumbnail($nextPost->ID, array(100, 100));
                                            } ?>
                                            <?php next_post_link('%link', "$nextthumbnail", TRUE); ?>
                                        </span>        
                                
                                <span class='next-link arrow<?php echo $targetNr+1; ?>'>
                                            <?php $prevPost = get_previous_post(true);

                                            if ($prevPost) {
                                                ?>

                                                <?php $prevthumbnail = get_the_post_thumbnail($prevPost->ID, array(100, 100));
                                            } ?>
                                            <?php previous_post_link('%link', "$prevthumbnail ", TRUE); ?>
                                        </span>
                                
                                        

                                    </div>

                                </div>

                    <?php };}; ?>

                </div>

            </div>

        </section>

<?php get_footer(); ?>