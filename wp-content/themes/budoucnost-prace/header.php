<?php
$kod_body = get_field('kod_body', 'options');
$kod_head = get_field('kod_head', 'options');
$logo_partner = get_field('logo_partner', 'options');
?>
<!DOCTYPE html>
<html>
<head>
    <?php if ($kod_head) echo $kod_head; ?>
    <title><?php wp_title('|', true, 'right'); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href="<?php echo get_template_directory_uri(); ?>/data/css/styles.css" rel="stylesheet" type="text/css">
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/data/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/data/js/modernizr.custom.90907.min.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/data/js/selectivizr.min.js"></script><![endif]-->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/data/js/respond.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/data/js/select2.min.js"></script>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/data/js/jquery.shuffleLetters_1.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/data/js/cookies.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/data/js/scripts.js"></script>
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo get_template_directory_uri(); ?>/data/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/data/favicon/favicon-32x32.png"
          sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/data/favicon/favicon-16x16.png"
          sizes="16x16">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/data/favicon/manifest.json">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/data/favicon/safari-pinned-tab.svg"
          color="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

    <script src="https://use.typekit.net/bxd1djw.js"></script>
    <script>try {
            Typekit.load({async: true});
        } catch (e) {
        }</script>
</head>
<body <?php body_class(); ?>>
<?php if ($kod_body) echo $kod_body; ?>
<header>
    <span class="burger"></span>
    <?php if ($logo_partner) { ?>
        <a href="<?php echo $logo_partner['description']; ?>" target="_blank" class="partner"><img src="<?php echo $logo_partner['url']; ?>" alt="<?php echo $logo_partner['title']; ?>"/></a>
    <?php }; ?>
</header>

<section class="fixed-menu"><span class="back-arrow"></span>
    <nav>
        <ul>
            <?php $arguments = array('posts_per_page' => -1);
            $list_posts = get_posts($arguments);
            foreach ($list_posts as $post) {
                setup_postdata($post);
                $pozice = get_field('pozice'); ?>
                <?php if (has_post_thumbnail()) {
                    $thumbnail_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail'); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <span class="image" style="background-image:url('<?php echo $thumbnail_image_url[0]; ?>');">
                                <span class="image-overlay"><b><?php the_title(); ?></b></span>
                            </span>
                            <?php if ($pozice) { ?>
                                <small class="item-text"><?php echo $pozice; ?></small>
                            <?php }; ?>
                        </a>
                    </li>
                <?php }; ?>
            <?php };
            wp_reset_postdata(); ?>
            <li>
                <a href="<?php echo get_site_url()?>/quiz">
                    <span class="image" style="background-image:url('<?php echo get_template_directory_uri(); ?>/data/images/quiz.jpg');">
                        <span class="image-overlay"><b>Otestuj sa</b></span>
                    </span>
                    <small class="item-text"><br><br></small>
                </a>
            </li>
        </ul>
    </nav>
</section>