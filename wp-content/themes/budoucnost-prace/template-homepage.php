﻿<?php

/*

Template Name: Domovská stránka

*/

?>

<?php get_header(); ?>

	<?php

		$logo_forbes = get_field('logo_forbes','options');

		$obrazky = get_field('obrazky');

	?>

        <section class="headline">

            <div class="headline-inner">

                <div class="headline-inner-cell">

				<?php if($logo_forbes) { ?>	

                    <a href="<?php echo $logo_forbes['description']; ?>" target="_blank" class="logo">

                        <img src="<?php echo $logo_forbes['url']; ?>" alt="<?php echo $logo_forbes['title']; ?>" />

                    </a>

                <?php }; ?>

                    <h1><?php bloginfo('name'); ?></h1>

                    <p class="description"><?php bloginfo('description'); ?></p>

                </div>

            </div>

            <?php if($obrazky) { ?>

                <a href="#prvni" class="scroll-down-image" data-interval="500">

                	<?php foreach($obrazky as $obrazek) { ?>

                    	<img src="<?php echo $obrazek['sizes']['large']; ?>" alt="<?php echo $obrazek['title']; ?>" />

                    <?php }; ?>

                </a>

            <?php }; ?>

        </section>

        

		<?php $arguments = array('posts_per_page' => -1);

        $list_posts = get_posts($arguments);

        $counter = 1; foreach($list_posts as $post) { setup_postdata($post);

			$pozice = get_field('pozice'); ?>

            <?php if($counter == 1) { ?>
                
                <section class="person fixed-menu-special">
    
                </section>

            <?php } ?>

            <section class="person"<?php if($counter == 1) { echo "id=\"prvni\""; }; ?>>

                <div class="container">

                    <div class="person-inner">

                        <div class="person-inner-cell">

                        	<?php if($pozice) { ?><h2><?php echo $pozice; ?></h2><?php }; ?>

							<?php if(has_post_thumbnail()) { $thumbnail_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail'); ?>

                            	<a href="<?php the_permalink(); ?>" class="image"><img src="<?php echo $thumbnail_image_url[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></a>

                            <?php }; ?>

                            <div class="job">

                            	<?php the_title(); ?>

                            </div>

                        </div>

                    </div>

                </div>

            </section>

        <?php $counter++; }; wp_reset_postdata(); ?>


        <section class="person credits" id="quiz-item">

            <div class="container">

                <div class="person-inner">

                    <div class="person-inner-cell">

                        <a href="<?php echo get_site_url()?>/quiz" class="image"><img src="<?php echo get_template_directory_uri(); ?>/data/images/quiz.jpg" alt=""></a>
                        <div class="job">Otestuj sa</div>

                    </div>

                </div>

                <div class="person-credits">
                    <p>Za projektom stoja</p>
                    <p><span class="l">Texty:</span> Lucia Vanková, Zuzana Matuščáková<br>
                        <span class="l">Ilustrácie:</span> Marek Mertinko<br>
                        <span class="l">Test:</span> Zuzana Reľovská/Burnout.sk</p>
                </div>

            </div>

        </section>


        <div class="persons-nav">

            <span class="prev"></span>

            <span class="next"></span>

        </div>

<?php get_footer(); ?>